var map = L.map("mapid", {
    minZoom: 2,
    maxZoom: 5,
    crs: L.CRS.Simple,
    attributionControl: false,
    zoomControl: false,
    preferCanvas: true,
    maxBoundsViscosity: .5
}).setView([-100, 100], 4);

var height = 25522;
var width = 27444;
var bounds = new L.LatLngBounds(map.unproject([0, height], 7), map.unproject([width, 0], 7));
map.setMaxBounds(bounds, {padding: [600, 600]});

var layer = L.tileLayer("https://kneebees-cdn.sirv.com/kneebees-map/tiles/{z}/{x}/{y}.png", {
    minZoom: 2,
    maxZoom: 5,
    bounds: bounds,
    noWrap: true,
    tms: false
}).addTo(map);

L.simpleGraticule({
    interval: 8.2,
    vinterval: 7.7,
    showOriginLabel: false,
    redraw: "move"
}).addTo(map);

var titleLayer = new L.LayerGroup();
map.addLayer(titleLayer);

var islands = [{
        "loc": [-41.819753270587555, 62.927539082707604],
        "title": "Ood Cove",
        radius: 3,
        pigs: true,
        snakes: true
    },
    {
        "loc": [-69.94175507751075, 50.54955884969927],
        "title": "Bird with Pipe",
        radius: 4,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-67.62877065330183, 65.30062001780092],
        "title": "Splatland",
        radius: 2,
        pigs: true
    },
    {
        "loc": [-80.94564356372827, 121.30273506665486],
        "title": "Lumpy Bean",
        radius: 3,
        chickens: true,
        snakes: true
    },
    {
        "loc": [-29.006074941430732, 18.117840198163094],
        "title": "Shattered Bow",
        radius: 4,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-21.065393452707312, 42.86583042922642],
        "title": "Bonnet Bay",
        radius: 4,
        chickens: true,
        snakes: true
    },
    {
        "loc": [-22.940799321639105, 57.487891142156535],
        "title": "Salty Kidneys",
        radius: 1.5,
        chickens: true
    },
    {
        "loc": [-28.629490743342114, 69.55120761078706],
        "title": "Rubber Duck",
        radius: 1.5,
        snakes: true
    },
    {
        "loc": [-28.19171579987219, 88.62081716840055],
        "title": "Clam Narwhal",
        radius: 1.5
    },
    {
        "loc": [-28.691820540241682, 107.0555642878491],
        "title": "Tailless Stingray",
        radius: 4,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-22.8781029334463, 120.48941102991489],
        "title": "Duck",
        radius: 1.5,
        snakes: true
    },
    {
        "loc": [-21.566440856402263, 136.5557290968507],
        "title": "Weird Lobster",
        radius: 3,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-32.44278938865952, 32.80535275921895],
        "title": "o)",
        radius: 1.5,
        snakes: true
    },
    {
        "loc": [-33.44299886939851, 50.23934822463562],
        "title": "Tiny Legless Dragon",
        radius: 1.5,
        chickens: true
    },
    {
        "loc": [-45.00792099044309, 24.432035940703415],
        "title": "Screaming Cyclops",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-48.25860180284481, 43.931357643687285],
        "title": "Irish Bagpipe",
        radius: 3,
        outpost: true
    },
    {
        "loc": [-56.135264556210544, 57.616119333511065],
        "title": "Smoking Alien",
        radius: 1.5,
        snakes: true
    },
    {
        "loc": [-55.3851074456563, 71.67579309594387],
        "title": "The other Screaming Cyclops",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-41.13192595693288, 114.67807920810932],
        "title": "Toilet Ski",
        radius: 1.5,
        pigs: true
    },
    {
        "loc": [-34.818103609768, 126.55069260749704],
        "title": "Screaming Beard",
        radius: 1.5,
        isFortress: true
    },
    {
        "loc": [-42.195761396643945, 137.43053681968217],
        "title": "Upset Carrot",
        radius: 2,
        pigs: true
    },
    {
        "loc": [-33.56895462527015, 144.36651233641732],
        "title": "Sniffy Hedgehog",
        radius: 1.5,
        chickens: true
    },
    {
        "loc": [-40.819360494201945, 93.80847591354413],
        "title": "Hairy Clam Shark",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-54.197162299085946, 105.7433527036019],
        "title": "Punching Rock Hand",
        radius: 4,
        outpost: true
    },
    {
        "loc": [-52.32176952270034, 125.36404020139321],
        "title": "Robo Wheel",
        radius: 2,
        pigs: true,
        snakes: true
    },
    {
        "loc": [-55.76110247916651, 145.1163475274157],
        "title": "Bow Lips",
        radius: 3,
        outpost: true
    },
    {
        "loc": [-64.51072835956577, 117.43038960965997],
        "title": "Angry Heron",
        radius: 1,
        pigs: true,
        snakes: true
    },
    {
        "loc": [-67.69269774083621, 130.11909434410913],
        "title": "Tard Cat",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-71.44348329360741, 142.9912651229149],
        "title": "Stabbed Fish",
        radius: 2,
        chickens: true
    },
    {
        "loc": [-67.50237, 151.430381],
        "title": "Baby Octopus",
        radius: 1,
        isSeapost: true
    },
    {
        "loc": [-19.313035, 27.037615],
        "title": "Rock Hat",
        radius: 1,
        isSeapost: true
    },
    {
        "loc": [-75.235017, 64.342312],
        "title": "Tiny Horsehead",
        radius: 1,
        isSeapost: true
    },
    {
        "loc": [-114.96965, 94.435731],
        "title": "Mustache Rock",
        radius: 1,
        isSeapost: true
    },
    {
        "loc": [-128.328685, 48.139087],
        "title": "Squirrel climbing a Stick",
        radius: 1,
        isSeapost: true
    },
    {
        "loc": [-29.844282, 120.967678],
        "title": "Squirrel on a Stick",
        radius: 1,
        isSeapost: true
    },
    {
        "loc": [-80.07029006498121, 135.55539947884748],
        "title": "Aardvark",
        radius: 2,
        pigs: true
    },
    {
        "loc": [-78.63248893641892, 152.05177368081218],
        "title": "Snooty Hedgehog",
        radius: 2,
        snakes: true
    },
    {
        "loc": [-92.63542166676478, 130.11909434410913],
        "title": "Floating Anchor",
        radius: 2,
        pigs: true
    },
    {
        "loc": [-92.19783001894149, 144.99082563224394],
        "title": "Beyblade",
        radius: 4,
        pigs: true,
        snakes: true
    },
    {
        "loc": [-74.94438667929433, 102.86928662127767],
        "title": "Pinball Island",
        radius: 4,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-58.448248980419464, 30.371595953863505],
        "title": "Tadpole",
        radius: 1,
        chickens: true
    },
    {
        "loc": [-66.31539345270731, 15.993498563600905],
        "title": "Pacman with one Tooth",
        radius: 3,
        pigs: true,
        snakes: true
    },
    {
        "loc": [-75.94243588991247, 31.746234927654953],
        "title": "Bean Birth",
        radius: 3,
        outpost: true
    },
    {
        "loc": [-81.88115349670785, 16.993169376947787],
        "title": "Kissy Shark",
        radius: 1.5,
        pigs: true
    },
    {
        "loc": [-81.6942164761939, 60.23384927983136],
        "title": "Turnip Turndown",
        radius: 2,
        chickens: true
    },
    {
        "loc": [-91.6963112835838, 48.48771722300551],
        "title": "Fat Ghost",
        radius: 4,
        chickens: true,
        snakes: true
    },
    {
        "loc": [-91.44625891339905, 30.743560286098365],
        "title": "Faceman",
        radius: 2,
        chickens: true,
        snakes: true
    },
    {
        "loc": [-97.13434807797746, 16.305895692771806],
        "title": "Hungry Fish",
        radius: 3,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-105.13603701643558, 36.36971243938428],
        "title": "Big Eyed Bird",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-103.69823588787327, 72.10794401653528],
        "title": "Us",
        radius: 1.5,
        pigs: true
    },
    {
        "loc": [-100.84721297846644, 83.52747464057205],
        "title": "Conda Isle",
        radius: 1,
        snakes: true
    },
    {
        "loc": [-104.19511986188078, 97.48786124216625],
        "title": "Fort Steak",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-117.31967471531044, 51.7447330135501],
        "title": "N",
        radius: 4,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-115.8779065452535, 69.80542248695795],
        "title": "Jerry Isle",
        radius: 2,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-115.25277561979163, 85.05040239049788],
        "title": "Henry Island",
        radius: 3,
        pigs: true,
        snakes: true
    },
    {
        "loc": [-116.75298510053062, 106.67686315627111],
        "title": "Giant Frog with Tooth",
        radius: 3,
        chickens: true,
        snakes: true
    },
    {
        "loc": [-114.69061602599248, 122.1182928219427],
        "title": "Bitten Potato",
        radius: 2,
        chickens: true
    },
    {
        "loc": [-126.31805123958326, 134.36426028544201],
        "title": "Rudeman",
        radius: 2,
        outpost: true
    },
    {
        "loc": [-124.56687291042644, 34.36631770202399],
        "title": "Boar Head",
        radius: 4,
        chickens: true,
        snakes: true
    },
    {
        "loc": [-137.44456997494095, 45.05029951966879],
        "title": "Spoon Wreck",
        radius: 2,
        chickens: true
    },
    {
        "loc": [-128.94278938865952, 65.35611291577732],
        "title": "Big Eyed Jim",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-144.821114895391, 60.10784114570619],
        "title": "Spiral of AAAAHHH",
        radius: 4,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-148.3847146692707, 73.99154401784801],
        "title": "Earplug",
        radius: 2,
        pigs: true
    },
    {
        "loc": [-152.51057877731904, 84.98792296466371],
        "title": "Elvis Isle",
        radius: 1.5,
        snakes: true
    },
    {
        "loc": [-134.88188667929433, 82.4887459312965],
        "title": "Cloudbean",
        radius: 2,
        outpost: true
    },
    {
        "loc": [-126.75507990792053, 95.36808708028451],
        "title": "Hairbrush",
        radius: 2,
        pigs: true
    },
    {
        "loc": [-133.19392844017779, 104.67752152957733],
        "title": "Boomerang Cay",
        radius: 1.5,
        snakes: true
    },
    {
        "loc": [-130.13078690541462, 117.3608449739159],
        "title": "Badger Fort",
        radius: 2,
        isFortress: true
    },
    {
        "loc": [-143.50858871029862, 113.17472344302583],
        "title": "Magic Fish",
        radius: 2,
        chickens: true
    },
    {
        "loc": [-143.75864108048336, 128.48218277239994],
        "title": "Sideways Brain",
        radius: 3,
        pigs: true,
        snakes: true
    },
    {
        "loc": [-147.8845051885317, 98.80445550116441],
        "title": "Tiny Headed Mermaid",
        radius: 4,
        chickens: true,
        pigs: true
    },
    {
        "loc": [-78.938966, 191.556523],
        "title": "Shark Arms",
        radius: 2,
        forsaken: true
    },
    {
        "loc": [-87.003155, 200.022485],
        "title": "Lady Hat",
        radius: 1,
        forsaken: true,
        isSeapost: true
    },
    {
        "loc": [-87.5329, 177.748416],
        "title": "Snailhelmet",
        radius: 4,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-96.568248, 198.610248],
        "title": "Donut",
        radius: 2,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-104.69495, 169.682274],
        "title": "Retarded Bird",
        radius: 2,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-107.257986, 185.614528],
        "title": "Hedgehog on a Skidoo",
        radius: 4,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-119.318876, 201.609261],
        "title": "Devil Cat Island",
        radius: 3,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-118.568719, 165.308714],
        "title": "The Hammer of Flames",
        radius: 1.5,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-127.003365, 178.80427],
        "title": "Crooked Ice Cream",
        radius: 3,
        forsaken: true,
        outpost: true,
        isSeapost: false
    },
    {
        "loc": [-136.81792, 198.36031],
        "title": "Clam Shark",
        radius: 1.5,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-137.943156, 212.605619],
        "title": "Foggy Island",
        radius: 1.5,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-144.69457, 179.491523],
        "title": "Long Neck Turtle",
        radius: 1.5,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-147.193195, 167.058118],
        "title": "Hatbird",
        radius: 1,
        forsaken: true,
        isSeapost: true
    },
    {
        "loc": [-152.506808, 204.045938],
        "title": "Octopus on a Rock",
        radius: 2,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-157.382829, 187.676328],
        "title": "Turtle humping a Volcano",
        radius: 3,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-159.258222, 167.432994],
        "title": "WHAAALE",
        radius: 1.5,
        forsaken: true,
        isSeapost: false
    },
    {
        "loc": [-172.636024, 175.305402],
        "title": "Angry Bean",
        radius: 3,
        forsaken: true,
        isSeapost: false
    }
];

for (var i in islands) {
    var name = islands[i].title;
    var loc = islands[i].loc;
    var rad = islands[i].radius;

    var marker = new L.marker(translate(loc, rad * 0.4, 0), {
        opacity: 0,
        fillOpacity: 0,
        fill: "false",
        interactive: false,
        title: name
    });
    marker.bindTooltip(name, { direction: "center", permanent: true, className: "island-name" });
    marker.addTo(titleLayer);
}

function translate(loc, dlat, dlng) {
    return [loc[0] + dlat, loc[1] + dlng];
}

map.on('zoomend', function() {
    var zoomLevel = map.getZoom();
    var tooltip = $('.island-name');

    switch (zoomLevel) {
        case 2:
            tooltip.css('display', 'none');
            break;
        case 3:
            tooltip.css('font-size', 8);
            tooltip.css('display', 'inline');
            break;
        case 4:
            tooltip.css('font-size', 10);
            tooltip.css('display', 'inline');
            break;
        case 5:
            tooltip.css('font-size', 20);
            tooltip.css('display', 'inline');
            break;
        default:
            tooltip.css('display', 'inline');
            tooltip.css('font-size', 20);
    }
});

var search = new L.Control.Search({
    position: 'topright',
    layer: titleLayer,
    initial: false,
    zoom: 4,
    marker: false
});
map.addControl(search);

